// vim: set expandtab fdm=marker ts=2 sw=2 tw=80 et :
/* extension.js
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

/* exported init */

const GLib = imports.gi.GLib;
const St = imports.gi.St;
const Lang = imports.lang;
const Main = imports.ui.main;
const Mainloop = imports.mainloop;
const PanelMenu = imports.ui.panelMenu;
const Util = imports.misc.util;

const timeout = 5;

let cmd_nordvpn_status =
`nordvpn status \
  | head -n1 \
  | awk -F: 'gsub(/ /,"") {print $2}' \
  | xargs -I{} test {} = "Connected"
`;

log(`Status: ${cmd_nordvpn_status}`)

const Indicator = new Lang.Class({
  Name: 'Nordvpn Indicator',
  Extends: PanelMenu.Button,
  icon: new St.Icon({ style_class: 'nordvpn-on-icon' }),

  _init: function() {
    // Init super
    this.parent(0.0, "Nordvpn Indicator", false);

    // Set timeout
    this.timeout_seconds = timeout;

    // Include child
    this.add_child(this.icon);

    // Refresh
    this._refresh();
  },

  _refresh: function () {
    // Spawn cmd to check if Nordvpn is active
    let [,,,code] = GLib.spawn_sync(null
      , [ '/bin/bash', '-c', cmd_nordvpn_status]
      , null
      , GLib.SpawnFlags.SEARCH_PATH
      , null
    )

    // Update icon based on status code
    if ( code == 0 ) {
      this.icon = new St.Icon({ style_class: 'nordvpn-on-icon' });
    }
    else {
      this.icon = new St.Icon({ style_class: 'nordvpn-off-icon' });
    }

    // Update child
    this.destroy_all_children();
    this.add_child(this.icon);

    // Handle timeout
    if (this._timeout) {
      Mainloop.source_remove(this._timeout);
      this._timeout = null;
    }

    this._timeout = Mainloop.timeout_add_seconds(this.timeout_seconds
      , Lang.bind(this, this._refresh)
    );

    return true;
  },

});

let menu;

function init() {}

function enable() {
  menu = new Indicator();
  Main.panel.addToStatusArea('nordvpn-indicator', menu);
}

function disable() {
  menu.destroy();
}
